<?php

namespace AzureSpring\Wxapi;

use AzureSpring\Wxapi\Model\Message;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;

class Messenger
{
    private $token;

    private $streamFactory;

    private $responseFactory;

    public function __construct(string $token, StreamFactoryInterface $streamFactory, ResponseFactoryInterface $responseFactory)
    {
        $this->token = $token;
        $this->streamFactory = $streamFactory;
        $this->responseFactory = $responseFactory;
    }

    /**
     * Echo.
     *
     * https://developers.weixin.qq.com/miniprogram/dev/framework/server-ability/message-push.html
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function copy(ServerRequestInterface $request): ResponseInterface
    {
        $this->check($request);

        return $this->ackn($request->getQueryParams()['echostr']);
    }

    public function recv(ServerRequestInterface $request): Message
    {
        $this->check($request);

        return Message::create(\GuzzleHttp\json_decode($request->getBody(), true));
    }

    /**
     * Acknowledge (success).
     *
     * @param string $body
     *
     * @return ResponseInterface
     */
    public function ackn(string $body = 'success'): ResponseInterface
    {
        return $this
            ->responseFactory
            ->createResponse()
            ->withBody($this->streamFactory->createStream($body))
        ;
    }

    public function check(ServerRequestInterface $request)
    {
        $q = $request->getQueryParams();
        $data = [$this->token, $q['timestamp'], $q['nonce']];
        sort($data, SORT_STRING);

        if (sha1(implode($data)) !== $q['signature']) {
            throw new Exception\SignatureException();
        }
    }
}
