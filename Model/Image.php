<?php

namespace AzureSpring\Wxapi\Model;

class Image implements Body
{
    const TYPE = 'image';

    private $id;

    private $url;

    /**
     * @param mixed $data
     *
     * @return Image
     */
    public static function create($data): Body
    {
        return new Image($data['MediaId'], $data['PicUrl']);
    }

    public function __construct(string $id, ?string $url = null)
    {
        $this->id = $id;
        $this->url = $url;
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function jsonSerialize()
    {
        return [
            'media_id' => $this->id,
        ];
    }
}
