<?php

namespace AzureSpring\Wxapi\Model;

class Text implements Body
{
    const TYPE = 'text';

    private $content;

    /**
     * @param mixed $data
     *
     * @return Text
     */
    public static function create($data): Body
    {
        return new Text($data['Content']);
    }

    public function __construct(string $content)
    {
        $this->content = $content;
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function jsonSerialize()
    {
        return [
            'content' => $this->content,
        ];
    }
}
