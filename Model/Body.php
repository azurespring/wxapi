<?php

namespace AzureSpring\Wxapi\Model;

interface Body extends \JsonSerializable
{
    public static function create($data): self;
    public function getType(): string;
}
