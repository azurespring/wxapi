<?php

namespace AzureSpring\Wxapi\Model;

class Notification implements \JsonSerializable
{
    const BRANCH_DEV = 'developer';
    const BRANCH_TEST = 'trial';
    const BRANCH_PROD = 'formal';

    /** @var string */
    private $to;

    /** @var string */
    private $template;

    /** @var array */
    private $data;

    /** @var string|null */
    private $page;

    /**
     * zh_CN|zh_HK|zh_TW|en_US.
     *
     * @var string|null
     */
    private $lang;

    /** @var string|null */
    private $branch;

    public function __construct(string $to, string $template, array $data, ?string $page = null, ?string $lang = null, ?string $branch = null)
    {
        $this->to = $to;
        $this->template = $template;
        $this->data = $data;
        $this->page = $page;
        $this->lang = $lang;
        $this->branch = $branch;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return string|null
     */
    public function getPage(): ?string
    {
        return $this->page;
    }

    /**
     * @param string|null $page
     *
     * @return $this
     */
    public function setPage(?string $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLang(): ?string
    {
        return $this->lang;
    }

    /**
     * @param string|null $lang
     *
     * @return $this
     */
    public function setLang(?string $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBranch(): ?string
    {
        return $this->branch;
    }

    /**
     * @param string|null $branch
     *
     * @return $this
     */
    public function setBranch(?string $branch): self
    {
        $this->branch = $branch;

        return $this;
    }

    public function jsonSerialize()
    {
        return array_filter([
            'touser' => $this->to,
            'template_id' => $this->template,
            'data' => array_map(
                function ($value) {
                    return [
                        'value' => $value,
                    ];
                },
                $this->data
            ),
            'page' => $this->page,
            'lang' => $this->lang,
            'miniprogram_state' => $this->branch,
        ]);
    }
}
