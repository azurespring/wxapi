<?php

namespace AzureSpring\Wxapi\Model;

class Link implements Body
{
    const TYPE = 'link';

    private $title;

    private $description;

    private $image;

    private $url;

    public static function create($data): Body
    {
        throw new \BadMethodCallException();
    }

    public function __construct(string $title, string $description, string $image, string $url)
    {
        $this->title = $title;
        $this->description = $description;
        $this->image = $image;
        $this->url = $url;
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function jsonSerialize()
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'thumb_url' => $this->image,
            'url' => $this->url,
        ];
    }
}
