<?php

namespace AzureSpring\Wxapi\Model;

class Message implements \JsonSerializable
{
    private $id;

    private $from;

    private $to;

    private $body;

    private $createdAt;

    public static function create($data): Message
    {
        switch ($data['MsgType']) {
            case Text::TYPE:
                $body = Text::create($data);
                break;

            case Image::TYPE:
                $body = Image::create($data);
                break;

            case Card::TYPE:
                $body = Card::create($data);
                break;

            case SessionStartEvent::TYPE:
                $body = SessionStartEvent::create($data);
                break;

            default:
                throw new \DomainException("Unknown message type: \"{$data['MsgType']}\"");
        }

        return new Message(
            @$data['MsgId'],
            $data['FromUserName'],
            $data['ToUserName'],
            $body,
            (new \DateTimeImmutable())->setTimestamp($data['CreateTime'])
        );
    }

    public function __construct(?string $id, ?string $from, string $to, Body $body, ?\DateTimeImmutable $createdAt)
    {
        $this->id = $id;
        $this->from = $from;
        $this->to = $to;
        $this->body = $body;
        $this->createdAt = $createdAt;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFrom(): ?string
    {
        return $this->from;
    }

    public function getTo(): string
    {
        return $this->to;
    }

    public function getBody(): Body
    {
        return $this->body;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function jsonSerialize()
    {
        return [
            'touser' => $this->to,
            'msgtype' => $this->body->getType(),
            $this->body->getType() => $this->body,
        ];
    }
}
