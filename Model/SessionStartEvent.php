<?php

namespace AzureSpring\Wxapi\Model;

class SessionStartEvent implements Body
{
    const TYPE = 'event';
    const NAME = 'user_enter_tempsession';

    private $from;

    public static function create($data): Body
    {
        if (self::NAME !== $data['Event']) {
            throw new \DomainException("Unknown event: \"{$data['Event']}\"");
        }

        return new SessionStartEvent($data['SessionFrom']);
    }

    public function __construct(string $from)
    {
        $this->from = $from;
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function getFrom(): string
    {
        return $this->from;
    }

    public function jsonSerialize()
    {
        throw new \BadMethodCallException();
    }
}
