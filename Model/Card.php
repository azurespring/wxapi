<?php

namespace AzureSpring\Wxapi\Model;

class Card implements Body
{
    const TYPE = 'miniprogrampage';

    private $app;

    private $path;

    private $title;

    private $image;

    /**
     * @param mixed $data
     *
     * @return Card
     */
    public static function create($data): Body
    {
        return new Card($data['AppId'], $data['PagePath'], $data['Title'], new Image($data['ThumbMediaId'], $data['ThumbUrl']));
    }

    public function __construct(string $app, string $path, string $title, Image $image)
    {
        $this->app = $app;
        $this->path = $path;
        $this->title = $title;
        $this->image = $image;
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function getApp(): string
    {
        return $this->app;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getImage(): Image
    {
        return $this->image;
    }

    public function jsonSerialize()
    {
        return [
            'title' => $this->title,
            'pagepath' => $this->path,
            'thumb_media_id' => $this->image->getId(),
        ];
    }
}
