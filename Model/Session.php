<?php

namespace AzureSpring\Wxapi\Model;

class Session
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $openId;

    /**
     * @var string|null
     */
    private $unionId;

    public function __construct(string $key, string $openId, ?string $unionId)
    {
        $this->key = $key;
        $this->openId = $openId;
        $this->unionId = $unionId;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     *
     * @return $this
     */
    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @return string
     */
    public function getOpenId(): string
    {
        return $this->openId;
    }

    /**
     * @param string $openId
     *
     * @return $this
     */
    public function setOpenId(string $openId): self
    {
        $this->openId = $openId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUnionId(): ?string
    {
        return $this->unionId;
    }

    /**
     * @param string|null $unionId
     *
     * @return $this
     */
    public function setUnionId(?string $unionId): self
    {
        $this->unionId = $unionId;

        return $this;
    }
}
