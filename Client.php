<?php

namespace AzureSpring\Wxapi;

use AzureSpring\Wxapi\Exception;
use AzureSpring\Wxapi\Model;
use GuzzleHttp\Client as Guzzle;

class Client
{
    private $appId;

    private $secret;

    private $guzzle;

    private $document;

    public function __construct(string $appId, string $secret, Guzzle $guzzle)
    {
        $this->appId = $appId;
        $this->secret = $secret;
        $this->guzzle = $guzzle;
    }

    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Get access token.
     *
     * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/access-token/auth.getAccessToken.html
     */
    public function getAccessToken(): string
    {
        $res = $this->guzzle->get('/cgi-bin/token', [
            'query' => [
                'grant_type' => 'client_credential',
                'appid' => $this->appId,
                'secret' => $this->secret,
            ],
        ]);

        $this->document = \GuzzleHttp\json_decode($res->getBody(), true);
        switch (@$this->document['errcode'] ?? 0) {
            case 0:
                return $this->document['access_token'];

            case -1:
                throw new Exception\SystemBusyException();

            case 40001:
            case 40013:
                throw new Exception\BadCredentialException();

            case 40002:
            default:
                throw new Exception\UnknownException($this->document['errmsg'], $this->document['errcode']);
        }
    }

    /**
     * Login.
     *
     * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/login/auth.code2Session.html
     *
     * @param string $code
     *
     * @return Model\Session
     *
     * @throws \Exception
     */
    public function getSession(string $code): Model\Session
    {
        $body = $this
            ->guzzle
            ->request('GET', '/sns/jscode2session', [
                'query' => [
                    'appid' => $this->appId,
                    'secret' => $this->secret,
                    'js_code' => $code,
                    'grant_type' => 'authorization_code',
                ],
            ])
            ->getBody()
            ->getContents()
        ;

        $this->document = \GuzzleHttp\json_decode($body);
        if (@$this->document->errcode) {
            throw new \Exception($this->document->errmsg, $this->document->errcode);
        }

        return new Model\Session($this->document->session_key, $this->document->openid, @$this->document->unionid);
    }

    /**
     * Decrypt.
     *
     * https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/signature.html#%E5%8A%A0%E5%AF%86%E6%95%B0%E6%8D%AE%E8%A7%A3%E5%AF%86%E7%AE%97%E6%B3%95
     *
     * @param string $data
     * @param string $iv
     * @param string $key
     *
     * @return mixed
     */
    public function decrypt(string $data, string $iv, string $key)
    {
        assert(24 === strlen($key));
        assert(24 === strlen($iv));

        $key = base64_decode($key);
        $iv = base64_decode($iv);
        $data = openssl_decrypt($data, 'AES-128-CBC', $key, 0, $iv);
        if (false === $data) {
            throw new \UnexpectedValueException();
        }

        $this->document = \GuzzleHttp\json_decode($data);
        if ($this->appId !== $this->document->watermark->appid) {
            throw new \UnexpectedValueException();
        }

        return $this->document;
    }

    /**
     * Push notification (subscription).
     *
     * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/subscribe-message/subscribeMessage.send.html
     */
    public function push(Model\Notification $notification, string $token)
    {
        $res = $this->guzzle->post('/cgi-bin/message/subscribe/send', [
            'query' => ['access_token' => $token],
            'body' => \GuzzleHttp\json_encode($notification, JSON_UNESCAPED_UNICODE),
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);

        $this->document = \GuzzleHttp\json_decode($res->getBody(), true);
        switch (@$this->document['errcode'] ?? 0) {
            case 0:
                break;

            case 40001:
            case 41001:
                throw new Exception\BadCredentialException();

            case 40003:
                throw new Exception\BadOpenIDException();

            case 43101:
                throw new Exception\UnsubscribedException();

            default:
                throw new Exception\UnknownException(@$this->document['errmsg'], @$this->document['errcode']);
        }
    }

    /**
     * Send.
     *
     * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/customer-message/customerServiceMessage.send.html#method-http
     *
     * @param Model\Message $message message
     * @param string        $token   access token
     */
    public function send(Model\Message $message, string $token)
    {
        $res = $this->guzzle->post('/cgi-bin/message/custom/send', [
            'query' => ['access_token' => $token],
            'body' => \GuzzleHttp\json_encode($message, JSON_UNESCAPED_UNICODE),
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);

        $this->document = \GuzzleHttp\json_decode($res->getBody(), true);
        switch (@$this->document['errcode'] ?? 0) {
            case 0:
                break;

            case -1:
                throw new Exception\SystemBusyException();

            case 40001:
                throw new Exception\BadCredentialException();

            case 40003:
                throw new Exception\BadOpenIDException();

            case 45015:
                throw new Exception\OutOfTimeException();

            case 45047:
                throw new Exception\OutOfLimitException();

            case 48001:
                throw new Exception\UnauthorizedException();

            case 40002:
            default:
                throw new Exception\UnknownException($this->document['errmsg'], $this->document['errcode']);
        }
    }

    /**
     * Upload.
     *
     * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/customer-message/customerServiceMessage.uploadTempMedia.html
     */
    public function load(string $filename, string $token)
    {
        $res = $this->guzzle->post('/cgi-bin/media/upload', [
            'query' => [
                'access_token' => $token,
                'type' => 'image',
            ],
            'multipart' => [[
                'name' => 'media',
                'contents' => fopen($filename, 'rb'),
            ]],
        ]);

        $this->document = \GuzzleHttp\json_decode($res->getBody(), true);
        if (@$this->document['errcode']) {
            return false;
        }

        return new Model\Image($this->document['media_id']);
    }
}
