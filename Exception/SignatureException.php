<?php

namespace AzureSpring\Wxapi\Exception;

class SignatureException extends \RuntimeException implements Exception
{
}
