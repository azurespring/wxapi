<?php

namespace AzureSpring\Wxapi\Exception;

class UnknownException extends \RuntimeException implements Exception
{
}
