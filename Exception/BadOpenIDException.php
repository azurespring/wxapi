<?php

namespace AzureSpring\Wxapi\Exception;

class BadOpenIDException extends \RuntimeException implements Exception
{
}
