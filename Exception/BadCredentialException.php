<?php

namespace AzureSpring\Wxapi\Exception;

class BadCredentialException extends \RuntimeException implements Exception
{
}
