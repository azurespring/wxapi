<?php

namespace AzureSpring\Wxapi\Exception;

class SystemBusyException extends \RuntimeException implements Exception
{
}
