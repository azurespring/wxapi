<?php

namespace AzureSpring\Wxapi\Exception;

class OutOfTimeException extends \RuntimeException implements Exception
{
}
