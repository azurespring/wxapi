<?php

namespace AzureSpring\Wxapi\Exception;

class UnauthorizedException extends \RuntimeException implements Exception
{
}
