<?php

namespace AzureSpring\Wxapi\Exception;

class OutOfLimitException extends \RuntimeException implements Exception
{
}
