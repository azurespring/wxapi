<?php

namespace AzureSpring\Wxapi\Exception;

class UnsubscribedException extends \RuntimeException implements Exception
{
}
